import React, { Component } from 'react';
import { readRemoteFile } from 'react-papaparse';
import ReactPlayer from 'react-player'

const public_spreadsheet_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vSoWh1W6g9-Zf_XknPqM8J-ArY0OjbdUYdHU8F-0ieAS23uvcA2jSIIoWPEe6Tz6XJPQpCEKRF4gSBJ/pub?gid=0&single=true&output=csv";

class DippedRigsList extends Component {
  constructor(props) {
    super(props);
    this.state = {rigs: []}
  }

  componentDidMount() {
    // console.log(this.props)
    this._fetch_rig_info();
  }

  _fetch_rig_info(params) {
    readRemoteFile(
      public_spreadsheet_url,
      {
        header: true,
        complete: (results) => {
          console.log('Results:', results)
          this._update_rigs_state(results.data);
        }
      }
    )
  }

  _update_rigs_state(rigData) {
    this.setState({rigs: rigData});
  }

  render() {
    return(
      <>
        {this.state.rigs.map((rig,id)=>{
          return <div className="my-12" key={id}>
            <h2 className="text-2xl ">Episode {rig.Episode} - {rig.Who}</h2>
            <ReactPlayer url={rig.YouTube} />
            <ul>
              <li>Rhett: {rig.Rhett}</li>
              <li>Zach: {rig.Zach}</li>
            </ul>
            <p><small>{rig.Notes}</small></p>
            <hr className="my-12" />
          </div>
        })}
      </>
    )
  }
}

export default DippedRigsList;