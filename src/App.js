import './App.css';
import DippedRigsList from './components/DippedRigsList';

function App() {
  return (
    <>
    <div class="lg:text-center">
      <p class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
        DIPPED IN TONE<br/>PODCAST
      </p>
      <h2 class="text-base  font-semibold tracking-wide uppercase">- Dipped Rigs -</h2>
      <p class="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">
      <a href="https://www.patreon.com/dippedintone" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-red-500 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">
        Support Dipped in Tone on PATREON
      </a>
        
      </p>
    </div>
    <div className="container md:mx-auto">
      <div className="flex flex-col justify-center">
        <div className="m-auto">
          <DippedRigsList/>
        </div>
      </div>
    </div>
    </>
  );
}

export default App;
